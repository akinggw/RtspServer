﻿#CROSS := 
#CROSS := arm-hisiv500-linux-
#CROSS := mipsel-linux-
CROSS := arm-himix200-linux-
#CROSS := 
CC := $(CROSS)gcc
CXX := $(CROSS)g++
AR := $(CROSS)ar

LIB = AFRtsp
LIBDIR = lib
TARGET = ./$(LIBDIR)/lib$(LIB).a
OBJSDIR = objs

TARGET1 = rtsp_server
TARGET2 = rtsp_pusher
TARGET3 = rtsp_h264_file
RTSPLDFLAGS = -L./$(LIBDIR) -l$(LIB)

INCLUDE = -I./inc/net -I./inc/xop
CXXFLAGS = -std=c++11
LDFLAGS = -lpthread
#LDFLAGS = -lrt -pthread -lpthread -ldl -lm
ARFLAGS = -rc

SRC1  = $(notdir $(wildcard ./src/net/*.cpp))
OBJS1 = $(patsubst %.cpp,$(OBJSDIR)/%.o,$(SRC1))

SRC2  = $(notdir $(wildcard ./src/xop/*.cpp))
OBJS2 = $(patsubst %.cpp,$(OBJSDIR)/%.o,$(SRC2))

all: BUILD_DIR $(TARGET) $(TARGET1) $(TARGET2) $(TARGET3)

BUILD_DIR:
	@-mkdir -p $(OBJSDIR) $(LIBDIR)

$(TARGET) : $(OBJS1) $(OBJS2)
	$(AR) $(ARFLAGS) $@ $(OBJS1) $(OBJS2)

$(TARGET1) : ./example/rtsp_server.cpp
	$(CXX) $< -o $@ $(RTSPLDFLAGS) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS)
$(TARGET2) : ./example/rtsp_pusher.cpp
	$(CXX) $< -o $@ $(RTSPLDFLAGS) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS)
$(TARGET3) : ./example/rtsp_h264_file.cpp
	$(CXX) $< -o $@ $(RTSPLDFLAGS) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS)
	
$(OBJSDIR)/%.o : ./src/net/%.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(INCLUDE)
$(OBJSDIR)/%.o : ./src/xop/%.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(INCLUDE)

.PHONY : clean
clean :
	-rm -rf $(OBJSDIR) $(LIBDIR) $(TARGET1) $(TARGET2) $(TARGET3)
